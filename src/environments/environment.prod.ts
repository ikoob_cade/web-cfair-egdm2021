export const environment = {
  production: true,
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  eventId: '610a4a3a39141300121705cf' // EGDM2021
};
