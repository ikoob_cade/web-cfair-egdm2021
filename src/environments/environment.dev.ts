export const environment = {
  production: true,
  base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1',
  socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',
  eventId: '610a4a3a39141300121705cf' // EGDM2021
};
