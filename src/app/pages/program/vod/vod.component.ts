import * as _ from 'lodash';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { VodService } from '../../../services/api/vod.service';
import { SpeakerService } from '../../../services/api/speaker.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-vod',
  templateUrl: './vod.component.html',
  styleUrls: ['./vod.component.scss']
})
export class VodComponent implements OnInit, AfterViewInit {

  public categories: Array<any>;
  public selectSpeaker: any;
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;

  constructor(
    private vodService: VodService,
    private speakerService: SpeakerService,
    public router: Router,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.loadVods();
  }

  ngAfterViewInit(): void {
    if (this.cookieService.get(`${environment.eventId}_vod_popup`)) {
      const cookieTime = this.cookieService.get(`${environment.eventId}_vod_popup`);
      const now = new Date().getTime().toString();
      if (cookieTime < now) {
        this.cookieService.delete(`${environment.eventId}_vod_popup`);
        this.entryAlertBtn.nativeElement.click();
      }
    } else {
      // ! 메인 팝업
      setTimeout(() => {
        this.entryAlertBtn.nativeElement.click();
      }, 0);
    }
  }

  /**
   * VOD 목록 조회
   */
  loadVods = () => {
    this.vodService.find().subscribe(res => {
      const categories = this.sortByCategory(res);
      categories.forEach((category) => {
        const groups = {};
        category.vods.forEach((vod) => {
          if (vod.group) {
            if (!groups[vod.group]) {
              groups[vod.group] = [vod];
            } else {
              groups[vod.group].push(vod);
            }
          }
        });
        const groupList = [];

        // tslint:disable-next-line: forin
        for (const i in groups) {
          groupList.push(groups[i]);
        }

        if (groupList.length > 0) {
          category.groups = groupList;
        }
      });
      this.categories = categories;
    });
  }

  /**
   * 각 VOD에 카테고리가 있을 경우 카테고리별로 정렬해서 return
   */
  sortByCategory = (vods) => {
    return _.chain(vods)
      .groupBy(vod => {
        return vod.category ? JSON.stringify(vod.category) : '{}';
      })
      .map((vod, category) => {
        category = JSON.parse(category);
        category.vods = vod;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }


  goDetail(vod): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

  /**
   * 발표자 LIVE / VOD 리스트 조회
   */
  getDetail = (selectSpeaker) => {
    this.speakerService.findOne(selectSpeaker.id)
      .subscribe(res => {
        this.selectSpeaker = res;
      });
  }

  noModal(): void {
    this.cookieService.set(`${environment.eventId}_vod_popup`, 'true', { expires: new Date(new Date().getTime() + 1000 * 60 * 60) });
  }
}
