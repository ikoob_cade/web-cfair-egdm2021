import * as _ from 'lodash';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PosterService } from '../../services/api/poster.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tpps',
  templateUrl: './tpps.component.html',
  styleUrls: ['./tpps.component.scss']

})
export class TppsComponent implements OnInit {

  public allPosters: Array<any>; // 전체 포스터 목록
  public allCategories: Array<any>; // 전체 카테고리 목록
  public categories: Array<any>; // 카테고리별 포스터 목록
  public posters: Array<any>; // 포스터 목록
  public searchText: string; // 검색어
  public searchType: string; // 검색 카테고리

  public ottCategories;
  public rhCategories;
  public hnCategories;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private posterService: PosterService,
  ) {
    this.searchType = '';
  }

  ngOnInit(): void {
    this.loadTpps();
  }

  /**
   * 포스터 목록 조회
   */
  loadTpps = () => {
    this.posterService.find('606e999126e0e7001366efe4', true).subscribe(res => {

      const filteredPosters = _.sortBy(res, 'subTitle'); // ex) PE01

      this.allPosters = filteredPosters;
      this.posters = filteredPosters;
      this.categories = this.sortByCategory(filteredPosters);
      this.allCategories = this.categories;

      this.ottCategories = _.filter(this.allPosters, poster => {
        return poster.subTitle.includes('OTTPP');
      });

      this.rhCategories = _.filter(this.allPosters, poster => {
        return poster.subTitle.includes('RHTPP');
      });
      this.hnCategories = _.filter(this.allPosters, poster => {
        return poster.subTitle.includes('HNTPP');
      });

    });
  }

  /**
   * 포스터 목록을 카테고리 별로 정제한다.
   * @param posters 포스터 목록
   */
  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.posters = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

  /** 카테고리를 선택한다. */
  selectCategory = (category) => {
    this.posters = category === 'all' ? this.allPosters : category.posters;
  }

  /** 카테고리 검색 */
  search = () => {
    const searchObject = this.searchType ? _.filter(this.allPosters, poster => {
      return poster.subTitle.includes(this.searchType);
    }) : this.allPosters;

    if (this.searchText) {
      if (this.searchType) {
        if (this.searchType === 'OTTPP') {
          this.ottCategories = _.filter(searchObject, poster => {
            return (poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
              poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
              poster.author.toLowerCase().includes(this.searchText.toLowerCase())
            );
          });
          this.rhCategories = [];
          this.hnCategories = [];
        } else if (this.searchType === 'RHTPP') {
          this.rhCategories = _.filter(searchObject, poster => {
            return (poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
              poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
              poster.author.toLowerCase().includes(this.searchText.toLowerCase())
            );
          });
          this.ottCategories = [];
          this.hnCategories = [];
        } else if (this.searchType === 'HNTPP') {
          this.hnCategories = _.filter(searchObject, poster => {
            return (poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
              poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
              poster.author.toLowerCase().includes(this.searchText.toLowerCase())
            );
          });
          this.ottCategories = [];
          this.rhCategories = [];
        }
      } else {
        this.ottCategories = _.filter(searchObject, poster => {
          if (!poster.subTitle.includes('OTTPP')) {
            return false;
          }
          return (poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
            poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
            poster.author.toLowerCase().includes(this.searchText.toLowerCase())
          );
        });
        this.rhCategories = _.filter(searchObject, poster => {
          if (!poster.subTitle.includes('RHTPP')) {
            return false;
          }
          return (poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
            poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
            poster.author.toLowerCase().includes(this.searchText.toLowerCase())
          );
        });
        this.hnCategories = _.filter(searchObject, poster => {
          if (!poster.subTitle.includes('HNTPP')) {
            return false;
          }
          return (poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
            poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
            poster.author.toLowerCase().includes(this.searchText.toLowerCase())
          );
        });
      }
    } else {
      if (this.searchType === 'OTTPP') {
        this.ottCategories = searchObject;
        this.rhCategories = [];
        this.hnCategories = [];
      } else if (this.searchType === 'RHTPP') {
        this.ottCategories = [];
        this.rhCategories = searchObject;
        this.hnCategories = [];
      } else if (this.searchType === 'HNTPP') {
        this.ottCategories = [];
        this.rhCategories = [];
        this.hnCategories = searchObject;
      } else {
        this.ottCategories = _.filter(this.allPosters, poster => {
          return poster.subTitle.includes('OTTPP');
        });
        this.rhCategories = _.filter(this.allPosters, poster => {
          return poster.subTitle.includes('RHTPP');
        });
        this.hnCategories = _.filter(this.allPosters, poster => {
          return poster.subTitle.includes('HNTPP');
        });
      }
    }

    // this.categories = this.sortByCategory(this.posters);
  }

  /** 포스터 상세로 페이지 이동 */
  goDetail(posterId: string): void {
    this.router.navigate([`tpps/${posterId}`]);
  }

}
