import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PosterService } from '../../../services/api/poster.service';

@Component({
  selector: 'app-tpps-detail',
  templateUrl: './tpps-detail.component.html',
  styleUrls: ['./tpps-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TppsDetailComponent implements OnInit {
  @ViewChild('pdfViewer') pdfViewer: any;

  posterId: string;
  selectedPoster: any = [];

  slide = null;
  slideCurrentPage = 1;
  slideTotalPage;

  constructor(
    public route: ActivatedRoute,
    private posterService: PosterService,
  ) {

    this.posterId = route.snapshot.params['tppId']
  }

  ngOnInit(): void {
    this.getPosterDetail();
  }

  getPosterDetail(): void {
    this.posterService.findOne(this.posterId).subscribe(res => {
      this.selectedPoster = res;
      if (this.selectedPoster.contents && this.selectedPoster.contents.contentUrl.includes('.pdf')) {
        this.slide = {
          url: this.selectedPoster.contents.contentUrl,
          credential: true,
        };
      }
    });
  }

  loadedSlide(event) {
    event.getPage(1);
    this.slideTotalPage = event._pdfInfo.numPages;
  }

  setSlide(amount: number) {
    if (this.slideCurrentPage <= this.slideTotalPage) {
      this.slideCurrentPage += amount;
    }
  }

}
