import { Injectable } from '@angular/core';

// 사용자 역할 확인 서비스
@Injectable()
export class RoleService {
    constructor() {
    }

    getRoles(userRoles): any {
        return {
            roleClientVod: userRoles ? userRoles.includes('ROLE_CLIENT_VOD') : false,
            roleClientDocument: userRoles ? userRoles.includes('ROLE_CLIENT_DOCUMENT') : false
        };
    }
}
